#Taller Área bajo la curva

#Variables
acum = 0
i=0

#Ingreso de datos
b = int(input("Ingrese el limite superior: "))
a = int(input("Ingrese el limite inferior: "))
n = int(input("Ingrese el numero de superintervalos: "))

#Delta x
T = (b - a) / n

while i <= n:
    x = (T*i) + a
    f = pow(x-1,2)+2    #La función (x-1)^2+2
    
    acum = acum + (f)   
    i = i + 1

acum = acum * T

print("El resultado aproximado es: ", acum)